SELECT reqChLg.orderId, reqr.FullName AS requestorsFullName, COUNT(orderStatelogId) AS timesRejected
FROM orderStateChangeLog reqChlg
RIGHT JOIN requestors reqr on reqChLg.requestorId = reqr.requestorId
WHERE reqChLg.orderstate = 'rejected'
GROUP BY reqChLg.orderId;