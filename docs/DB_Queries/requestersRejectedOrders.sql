SELECT reqr.requestorId, reqr.name AS reuqestorsName, COUNT(orderState) AS rejectedOrders
FROM requestors reqr
LEFT JOIN orders ord on reqr.requestorId = ord.requestorId
WHERE ord.orderstate = 'rejected'
GROUP BY reqr.requestorId;